<?php 

add_action('add_meta_boxes', '___register_product_parts_meta_box');
function ___register_product_parts_meta_box(){
	add_meta_box( 'product_parts_section', __('Product parts', 'woo-product-parts'), '___product_parts_callback', ['product'] );
}

function ___product_parts_callback( $post, $meta ){

	$screens = $meta['args'];
    wp_nonce_field( plugin_basename(__FILE__), 'product_parts_noncename' ); 

    $terms = wp_list_pluck(get_terms([ 'taxonomy' => 'parts', 'hide_empty' => false ]), 'name', 'id'); ?>
    <style>
        #product_parts_section .parts_select {
            width: 65%;
        }
        #product_parts_section .parts_count {
            width: 25%;
            padding: 3px 3px 4px 3px;
        }
        #product_parts_section .repeater-field {
            margin-bottom: 7px;
        }
        #product_parts_section .repeater-field:last-child {
            margin-bottom: unset;
        }
        #product_parts_section .add-btn {
            margin-bottom: 7px;
        }
    </style>
    <script>
        jQuery('body').on('click', '.add-btn', function(e){

            var $fields = $('.repeater-field');
            var length = [];
            if($fields.length != 0){
                $.each($fields, function( key, value ) {
                    $select = $(value).find('select');

                    var regex = /\[(\d+)\]/gi;
                    match = regex.exec($select.attr('name'));
                    length.push(match[1]);
                });
            }

            var html = `<div class="form-group repeater-field">
                            <select name="product_parts_id[` + length.length + `]" class="parts_select">
                                <option><?php _e('Select part', 'woo-product-parts'); ?></option>
                                <?php foreach($terms as $id => $name){ ?>
                                <option value="<?= $id; ?>"><?= $name; ?></option>
                                <?php } ?>
                            </select>
                            <input type="text" name="product_parts_value[` + length.length + `]" value="" class="parts_count" placeholder="<?php _e('Enter count', 'woo-product-parts'); ?>" />
                            <button type="button" class="button remove-btn"><?php _e('Delete', 'woo-product-parts'); ?></button>
                        </div>`;

            if($fields.length != 0){
                $fields.parent().append(html)
            } else {
                $(this).parent().append(html)
            }
        });

        jQuery('body').on('click', '.remove-btn', function(e){
            $(this).parent('.repeater-field').remove();
        });
    </script>

    <button type="button" class="button add-btn"><?php _e('Add part', 'woo-product-parts'); ?></button>
    <?php if(!empty($terms)) { ?>
    <div class="form-group repeater-field">
        <select name="product_parts_id[0]" class="parts_select">
            <option><?php _e('Select part', 'woo-product-parts'); ?></option>
            <?php foreach($terms as $id => $name){ ?>
            <option value="<?= $id; ?>"><?= $name; ?></option>
            <?php } ?>
        </select>
        <input type="text" name="product_parts_value[0]" value="" class="parts_count" placeholder="<?php _e('Enter count', 'woo-product-parts'); ?>" />
        <button type="button" class="button remove-btn"><?php _e('Delete', 'woo-product-parts'); ?></button>
    </div>
    <?php } ?>


    <!--div class="form-group repeater-field">
        <select name="product_parts_id[0]" class="parts_select">
            <option>Роза цветная</option>
        </select>
        <input type="text" id= "myplugin_new_field" name="product_parts_value[0]" value="10" class="parts_count" placeholder="<?php _e('Enter count', 'woo-product-parts'); ?>" />
        <button type="button" class="button remove-btn">-</button>
        <button type="button" class="button add-btn">+</button>
    </div>
    <div class="form-group repeater-field">
        <select name="product_parts_id[1]" class="parts_select">
            <option><?php _e('Select part', 'woo-product-parts'); ?></option>
        </select>
        <input type="text" id= "myplugin_new_field" name="product_parts_value[1]" value="" class="parts_count" placeholder="<?php _e('Enter count', 'woo-product-parts'); ?>" />
        <button type="button" class="button add-btn">+</button>
    </div-->

<?php }


add_action( 'save_post', 'myplugin_save_postdata' );
function myplugin_save_postdata( $post_id ) {
	// Убедимся что поле установлено.
	if ( ! isset( $_POST['myplugin_new_field'] ) )
		return;

	// проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
	if ( ! wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename(__FILE__) ) )
		return;

	// если это автосохранение ничего не делаем
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
		return;

	// проверяем права юзера
	if( ! current_user_can( 'edit_post', $post_id ) )
		return;

	// Все ОК. Теперь, нужно найти и сохранить данные
	// Очищаем значение поля input.
	$my_data = sanitize_text_field( $_POST['myplugin_new_field'] );

	// Обновляем данные в базе данных.
	update_post_meta( $post_id, '_my_meta_value_key', $my_data );
}