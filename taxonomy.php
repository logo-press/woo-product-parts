<?php

add_action( 'init', 'create_book_taxonomies' );

function create_book_taxonomies(){

	register_taxonomy('parts', ['product'], array(
		'hierarchical'  => false,
		'labels'        => array(
			'name'              => _x( 'Parts', 'woo-product-parts' ),
			'singular_name'     => _x( 'Part', 'woo-product-parts' ),
			'search_items'      => __( 'Search Parts', 'woo-product-parts' ),
			'all_items'         => __( 'All Parts', 'woo-product-parts' ),
			'parent_item'       => __( 'Parent Part', 'woo-product-parts' ),
			'parent_item_colon' => __( 'Parent Part:', 'woo-product-parts' ),
			'edit_item'         => __( 'Edit Part', 'woo-product-parts' ),
			'update_item'       => __( 'Update Part', 'woo-product-parts' ),
			'add_new_item'      => __( 'Add New Part', 'woo-product-parts' ),
			'new_item_name'     => __( 'New Part Name', 'woo-product-parts' ),
			'menu_name'         => __( 'Parts', 'woo-product-parts' ),
		),
		'show_ui'       => true,
		'query_var'     => true,
	));
}

add_action( 'init', '___register_parts_meta' );
function ___register_parts_meta() {
    register_meta( 'term', '__parts_price', '___sanitize_parts_meta' );
}

function ___sanitize_parts_meta( $value ) {
    return sanitize_text_field($value);
}

function ___get_parts_price( $term_id ) {
    $value = get_term_meta( $term_id, '__parts_price', true );
    return ___sanitize_parts_meta( $value );
}

add_action( 'parts_add_form_fields', '___add_parts_price_form' );
function ___add_parts_price_form() { ?>
    <?php wp_nonce_field( basename( __FILE__ ), 'parts_price_nonce' ); ?>
    <div class="form-field parts-price">
        <label for="parts-price"><?php _e( 'Parts price', 'woo-product-parts' ); ?></label>
        <input type="text" name="parts_price" id="parts-price" value="" class="parts-price-field" />
    </div>
<?php }

add_action( 'parts_edit_form_fields', '___edit_parts_price_form' );
function ___edit_parts_price_form( $term ) {

    $value = ___get_parts_price( $term->term_id );

    if ( ! $value )
        $value = ""; ?>

    <tr class="form-field parts-price">
        <th scope="row">
            <label for="parts-price"><?php _e( 'Parts price', 'woo-product-parts' ); ?></label>
        </th>
        <td>
            <?php wp_nonce_field( basename( __FILE__ ), 'parts_price_nonce' ); ?>
            <input type="text" name="parts_price" id="parts-price" value="<?php echo esc_attr( $value ); ?>" class="parts-price-field" />
        </td>
    </tr>
<?php }

add_action( 'edit_parts',   '___save_parts_price' );
add_action( 'create_parts', '___save_parts_price' );

function ___save_parts_price( $term_id ) {

    if ( ! isset( $_POST['parts_price_nonce'] ) || ! wp_verify_nonce( $_POST['parts_price_nonce'], basename( __FILE__ ) ) )
        return;

    $old_value  = ___get_parts_price( $term_id );
    $new_value = isset( $_POST['parts_price'] ) ? ___sanitize_parts_meta ( $_POST['parts_price'] ) : '';

    if ( $old_value && $new_value === '' )
        delete_term_meta( $term_id, '__parts_price' );

    else if ( $old_value !== $new_value )
        update_term_meta( $term_id, '__parts_price', $new_value );
}