<?php 
// === Woo Product Parts ===
// Plugin Name: Woo Product Parts
// Plugin URI: https:/trilogo.dp.ua/plugins/woo-product-parts
// Description: Просто плагин для комбинирование товара из частей. При обновление которых формируеться цена продукта
// Version: 0.0.1
// Author: Имя LorDee
// Author URI: https:/trilogo.dp.ua/team/LorDee
// Contributors: LorDee
// Tags: woocommerce, product, parts
// Requires at least: 3.6.2
// Tested up to: 3.6.2
// Requires PHP: 5.6
// Stable tag: 5.6
// License: GPLv3 or later License
// URI: http://www.gnu.org/licenses/gpl-3.0.html

/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    include(plugin_dir_path( __FILE__ ) . 'taxonomy.php');
    include(plugin_dir_path( __FILE__ ) . 'product.php');
} else {
    add_action( 'admin_notices', 'woo_check_alert' );
}

function woo_check_alert() {
    ?>
    <div class="notice notice-error is-dismissible">
        <p><strong><?php _e( 'Woo Product Parts', 'woo-product-parts' ); ?></strong></p>
        <p><?php _e( 'Woocommerce is required for plugin operation!', 'woo-product-parts' ); ?></p>
    </div>
    <?php
}
